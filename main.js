window.addEventListener('load', function() {

  var webAuth = new auth0.WebAuth({
    domain: 'intechwebprog.eu.auth0.com',
    clientID: 'vuwl5yj5cx3x9NBVMY4BAuFUeIrsORsE',
    responseType: 'token id_token',
    audience: 'https://intechwebprog.eu.auth0.com/userinfo',
    scope: 'openid',
    redirectUri: 'http://intechwebprog.azurewebsites.net/todo.html'
  });
  
  var loginBtn = document.getElementById('btn-login');
  var logoutBtn = document.getElementById('btn-logout');
  var addBtn = document.getElementById('btn-add');  
  var addTest = document.getElementById('btn-test');

  if(loginBtn != null) {
    loginBtn.addEventListener('click', function(e) {
      e.preventDefault();
      webAuth.authorize();
    });
  }

  if(logoutBtn != null) {
    logoutBtn.addEventListener('click', logout);
  }

  if(addBtn != null) {
    addBtn.addEventListener('click', submit);
  }
  if(addTest != null) {
    addTest.addEventListener('click', test);
  }
  function handleAuthentication() {
    webAuth.parseHash(function(err, authResult) {
      if (authResult && authResult.accessToken && authResult.idToken) {
        setSession(authResult);
      } else if (err) {
        console.log(err);
        alert(
          'Error: ' + err.error + '. Check the console for further details.'
        );
      }
    });
  }
  
  function setSession(authResult) {
    var expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    );
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);

    if(isAuthenticated() == true) {	
      var config = {
        apiKey: "AIzaSyBhQJ3jyPva6FInn7YxSbAWl0N88TCRvRo",
        authDomain: "users-26750.firebaseio.com",
        databaseURL: "https://users-26750.firebaseio.com",
        storageBucket: "gs://users-26750.appspot.com"
      };
      firebase.initializeApp(config);
      var database = firebase.database();
    }

    var userID = JSON.parse(atob(localStorage.getItem('id_token').split('.')[1])).sub.split('|')[1]
    firebase.database().ref('Users/' + userID).once('value').then(function(snapshot) {
        snapshot.forEach(function(child) { add_note(child.val().Text,child.val().Text2) })
    })
  }

  function test() {
   window.location.replace("http://intechwebprog.azurewebsites.net/qunit.html");
  }
  function logout() {
   localStorage.removeItem('access_token');
   localStorage.removeItem('id_token');
   localStorage.removeItem('expires_at');
   window.location.replace("http://intechwebprog.azurewebsites.net");
  }

  function isAuthenticated() {
    var expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime()< expiresAt;
  }

   if(isAuthenticated() == false && window.location.href != 'https://intechwebprog.azurewebsites.net/') {
    handleAuthentication()
  }

  function submit() {
	var time;
	var fin;
	var fin2;
    var NoteText = document.getElementById('note_text').value
	getJSON('https://wt-0a432cfb0dd052c849ad8b8f189efc3a-0.run.webtask.io/adsnlasjdk', 
	function(err, data) { 
		if (err !== null) { 
			alert('Time not defined : ' + err);
			return 0;
	} else { 
		time = data.currentTime;
		fin="Time of addition: "+time.toString();
		fin2="Text of task: "+NoteText.toString()
		if (NoteText.replace(/\s/g, '').length) {
			if(isAuthenticated() == true) {
				var userID = JSON.parse(atob(localStorage.getItem('id_token').split('.')[1])).sub.split('|')[1]
				firebase.database().ref('Users/' + userID).push({
					Text: fin,
					Text2: fin2
				});
				add_note(fin,fin2)
			}
		}	
		else{
			alert("Empty string");
		}
	} 
	} 
	);
  }

  function add_note(noteText,noteText2) {
	document.getElementById("addPole").value +="\n"+noteText+"\n"+noteText2;
  }
  
  var clearBtn = document.getElementById('btn-clear');
  if(clearBtn != null) {
	clearBtn.addEventListener('click', deleteElement)
  } 
  function deleteElement() {
  var userID = JSON.parse(atob(localStorage.getItem('id_token').split('.')[1])).sub.split('|')[1]
  firebase.database().ref('Users/' +userID).remove();  
  document.getElementById("addPole").value ="";
}
var timeBtn = document.getElementById('btn-time'); 
var timeSpan = document.getElementById('time-show'); 

if(timeBtn != null) {
timeBtn.addEventListener('click', fght)
}
function fght(){ 
	timeSpan.innerHTML = 'Adventure time!'; 
	getJSON('https://wt-0a432cfb0dd052c849ad8b8f189efc3a-0.run.webtask.io/adsnlasjdk', 
	function(err, data) { 
		if (err !== null) { 
		alert('Something went wrong: ' + err); 
	} else { 
		timeSpan.innerHTML = data.currentTime; 
		} 
	} 
	); 
};
function getJSON (url, callback) { 
	var httpRequest = new XMLHttpRequest(); 

	httpRequest.open('GET', url, true); 
	httpRequest.responseType = 'json'; 
	httpRequest.onload = function() { 
		var status = httpRequest.status; 
		if (status === 200) { 
			callback(null, httpRequest.response); 
		} else { 
			callback(status, httpRequest.response); 
		} 
	}; 
	httpRequest.send(); 
}; 
});