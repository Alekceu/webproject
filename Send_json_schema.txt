{
	"type":"object",
	"$schema": "http://json-schema.org/draft-03/schema",
	"required":true,
	"properties":{
		"List": {
			"type":"object",
			"required":true,
			"properties":{
				"UserID": {
					"type":"object",
					"required":true,
					"properties":{
						"UserNum": {
							"type":"string",
							"required":true
						},
						"TimeAddTask": {
							"type":"string",
							"required":true
						}
						"UserTask": {
							"type":"string",
							"required":true
						}
					}
				}
			}
		}
	}
}
